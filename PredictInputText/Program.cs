﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Threading;
using System.Collections.Concurrent;

namespace PredictInputText
{
	class MainClass
	{

		public static void Main (string[] args)
		{
			Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

			int N;
			int M;
			Dictionary<string, int> words = new Dictionary<string, int>();
			List<string> inputs = new List<string> ();
			HashSet<string> inp = new HashSet<string> ();
			ConcurrentDictionary<string, string> used = new ConcurrentDictionary<string, string> ();
			StreamReader sr;

			Stopwatch stopWatch = Stopwatch.StartNew();

			sr = new StreamReader (Console.OpenStandardInput(1789346));

//			FileStream fs = File.Open (@"test.in", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
//			BufferedStream bs = new BufferedStream (fs, 1789346);
//			sr = new StreamReader (bs);


			string number = string.Empty;
			if ((number = sr.ReadLine ()) != null) {
				int.TryParse (number, out N);
				words = new Dictionary<string, int> (N);

				string s = String.Empty;
				for (int i = 0; i < N; i++)
				{
					if ((s = sr.ReadLine ()) != null) {
						var ss = s.Split (' ');
						words.Add (ss [0], int.Parse (ss[1]));
					}
				}

				if ((s = sr.ReadLine ()) != null) {
					M = int.Parse (s);

					for (int i = 0; i < M; i++)
					{
						if ((s = sr.ReadLine ()) != null) {
							inputs.Add (s);
							inp.Add (s);
						}
					}
				}
			}
			var ot = new string[inputs.Count];
			Parallel.For(0, inputs.Count, (i) => {
				if (!used.ContainsKey(inputs[i])) {
					var result = words.Where (kvp => kvp.Key.StartsWith (inputs[i], StringComparison.Ordinal));
					if (result.Any ()) {
						var res = result.OrderByDescending (kvp => kvp.Value).ThenBy(kvp => kvp.Key).Select (kvp => kvp.Key).Take (10);
						ot[i] = string.Join ("\n", res);
						used.TryAdd(inputs[i], ot[i]);
					}
				} else {
					ot[i] = used[inputs[i]];
				}
			}
			);
			var output = ot.Where(e => e != null);
			Console.Out.WriteLine (string.Join ("\n\n", output));
//			Console.Out.WriteLine(output.ToArray().Length);

//			Console.WriteLine (stopWatch.Elapsed);
		}
	}
}
